"use strict";
let playerNick , opponentNick , playerPassword , mulitplayer, game;


function drawMultiplayerHTML(player){
    document.getElementById("player-turn").innerHTML = player + "' turn";
}

function drawLoggedIn(nick){
    document.getElementById("logged-in").innerHTML = "Welcome to mancala " + nick;
}

function drawWaitingForPlayerStatus(){
    document.getElementById("player-turn").innerHTML = "Waiting for players";
}

function saveScores(playerScore,opponentScore){
    document.getElementById("classificacoes").append(playerNick + " : " + playerScore + " - ", opponentNick + " : " + opponentScore,"\n");
}

function resetMultiplayerStats(){
    opponentNick = undefined;
    multiplayer = false;
    game = undefined;
}

async function getAuthentication(){

    playerNick = document.getElementById("username").value;
    playerPassword = document.getElementById("password").value;
    let port = document.getElementById("port").value;
    const response = await register(playerNick,playerPassword,port);
    if(response != "Login Successfully"){
        alert(response);
        return;
    }
    drawLoggedIn(playerNick);
}

async function syncMancala(info){
    let gameData = JSON.parse(info.data);
    if(gameData.winner !== undefined){
        //if someone left
        if(gameData.stores == undefined){
            alert(gameData.winner == playerNick ? "Won" : "Lost");
            deleteMancala();
            resetMultiplayerStats();
            return;
        }
        saveScores(gameData.stores[playerNick],gameData.stores[opponentNick]);
        if(gameData.winner == null){
            mancala.endGame();
            alert("Draw");
            deleteMancala();
            resetMultiplayerStats();
            return;
        }
        if(gameData.winner){
            mancala.endGame();
            alert(gameData.winner == playerNick ? "Won" : "Lost");
            deleteMancala();
            resetMultiplayerStats();
            return;
        }
    }

    drawMultiplayerHTML(gameData.board.turn);

    
    //setup time
    if(opponentNick == undefined){
        createMancala();
        let firstPlayer = gameData.board.turn;
        if(firstPlayer != playerNick){ //if we're not the first ones to play
            opponentNick = firstPlayer;
        }
        else{
            Object.keys(gameData.stores).forEach(player =>{
                if(player != playerNick)
                    opponentNick = player;
            });
        }
        
    }
    if(gameData.pit != undefined){
        mancala.currentPlayer ? mancala.sow(mancala.player.row.holes[gameData.pit]) : mancala.sow(mancala.opponent.row.holes[mancala.numHoles - gameData.pit - 1]);
    }
    mancala.updateMancala();
}

async function initMultiplayer(){
    let configuration = new Configuration();
    game = await join(playerNick,playerPassword,Number(configuration.numHoles),Number(configuration.numSeeds));
    multiplayer = true;
    update(playerNick,game,syncMancala);
    drawWaitingForPlayerStatus();
}


async function rankingMancala(){
    let leaderboard = document.getElementById("classificacoes");
    let rank;
    //table created
    var table = document.createElement("TABLE");
    leaderboard.appendChild(table);

    //procedemos a criar a primeira row com o Nick, Games e Victories
    var firstRow = document.createElement("TR");
    table.id = "ranking-table";
    table.appendChild(firstRow);
    let port = document.getElementById("port").value;
    //agora dar fill da row
    rank = (await ranking(`http://twserver.alunos.dcc.fc.up.pt:${port}/ranking`)).ranking;
    let firstRowKeys = Object.keys(rank[0]);
    for(let i = 0 ; i < firstRowKeys.length; i++){
        var foo = document.createElement("TD");
        var fooContent  = document.createTextNode(firstRowKeys[i]);
        foo.appendChild(fooContent);
        firstRow.appendChild(foo);
    }

    for(let i = 0; i < rank.length; i++){
        var row = document.createElement("TR");
        table.appendChild(row);
        for(let j = 0; j < firstRowKeys.length;j++){
            var foo = document.createElement("TD");
            var fooContent = document.createTextNode(rank[i][firstRowKeys[j]]);
            foo.appendChild(fooContent);
            row.appendChild(foo);
        }
    }

    

}

async function disconnectMultiplayer(){
    leave(playerNick,playerPassword,game);
}

async function playSeedServer(i){
    //normalizing
    if(mancala.currentPlayer == 0){
        i = mancala.numHoles - i - 1;
    }
    let response = await notify(playerNick,playerPassword,i,game);
}
