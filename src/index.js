const http = require('http');
const url = require("url");
const hostname = 'twserver.alunos.dcc.fc.up.pt';
const port = 9100;
const fs = require('fs');
const crypto = require('crypto');

function readJson(filePath, data) {
    fs.readFile(filePath, (err, fileData) => {
        if (err) {
            return data && data(err);
        }
        try {
            const object = JSON.parse(fileData);
            return data && data(null, object);
        } catch (err) {
            return data && data(err);
        }
    })
}

//{"group": 99, "nick": "zp", "password": "secret", "size": 6, "initial" 4 } 
//only called if register is sucessful
function join(request,response) {
    let msg = "";
    
    request
        .on("data",(data) => msg += data)
        .on("end",() => {
            let object = JSON.parse(msg);
            let hash = (object.group * object.group.toString().size).toString() + (object.size * object.size.toString().size).toString() + (object.initial * object.initial.toString().size).toString();
            const gameId = crypto.createHash('md5')
                                .update(hash)
                                .digest('hex');
            
            console.log(gameId);
            response.writeHead(200,{"Access-Control-Allow-Origin": "*",'Content-Type': 'application.json'});
            response.end(JSON.stringify({"game" : gameId}));
            })
        
}


function ranking(request, response) {
    request
        .on("data", (data) => null)
        .on("end", () => {
            readJson('./ranking', (err, ranking) => {
                if (ranking == undefined){
                    return;
                }
                response.writeHead(200, { "Access-Control-Allow-Origin": "*",'Content-Type': 'application.json' });
                response.end(JSON.stringify(ranking));
            })
        })
}


function register(request, response) {
    let msg = "";
    request
        .on("data", (data) => msg += data)
        .on("end", () => {
            let object = JSON.parse(msg);
            let nick = object.nick;
            let password = object.password;
            let passwordHash = crypto.createHash('md5').update(password).digest('hex'); //vai comparar com a password depois do hash
            readJson('./credenciais', (err, users) => {
                let statusCode;
                let answer = {};
                if (users == undefined)
                    return;
                

                if (nick in users) //se existe esse utilizador
                {
                    if (users[nick] == passwordHash) { //se a password for 
                        statusCode = 200;
                        answer = {}
                    }
                    else { //caso seja diferente da uma resposta diferente
                        statusCode = 401;
                        answer["error"] = "User registered with a different password";
                    }
                }
                else //caso nao exista o utilizador
                {
                    users[nick] = passwordHash;
                    fs.writeFile('./credenciais', JSON.stringify(users), (err) => {  //cria o utilizador, tem de se ver como cria o modelo com as definicoes certas
                        if (err) {
                            throw new Error('Erro ao ler o ficheiro.');
                        }
                    })
                    statusCode = 200;
                    answer = {};
                }
                response.writeHead(statusCode, { "Access-Control-Max-Age": 2592000, "Access-Control-Allow-Methods": "OPTIONS, POST, GET","Access-Control-Allow-Origin": "*",'Content-Type': 'text/plain' }); 
                response.end(JSON.stringify(answer));
            })
        })
}

//curl -H 'Content-Type: application/json' -d '{"group": 99, "nick": "zp", "password": "secret", "size": 6, "initial" 4 }' --request POST --url http://127.0.0.1:9100/join
//curl -H 'Content-Type: application/json' -d '{""nick": "zp", "password": "secret"}' --request POST --url http://127.0.0.1:9100/register
//

const server = http.createServer((req, res) => {
    const parsedURL = url.parse(req.url, true);
    const path = parsedURL.pathname;
    switch (path) {
        case "/register":
            if (req.method === "POST") register(req, res);
            break;
        case "/ranking":
            if (req.method === "POST") ranking(req, res);
            break;
            
        case "/join":
            if (req.method == "POST") join(req,res);
            break;

        default:
            res.writeHead(404, { 'Content-Type': 'text/plain' });
            res.end('Invalid Command \n');
    }
});


server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
