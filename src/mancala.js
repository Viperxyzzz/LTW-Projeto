"use strict";

function changeView(id){
    var element = document.getElementById(id);
    element.style.display == 'none' || element.style.display == ' ' ? element.style.display = 'block' : element.style.display = 'none';
}

function getRandomNumber(min, max) {
    
    return Math.random() * (max - min) + min;
          
}


function removeHovers(element){
    let toDelete = []
    for(let i = 0 ; i < element.children.length; i++){
        if(element.children[i].tagName == "CANVAS"){
            toDelete.push(element.children[i]);
        }
    }
    for(let i = 0; i < toDelete.length; i++){
        element.removeChild(toDelete[i]);
    }

}


function drawCanvasSeeds(element,length){
    const canvas = document.createElement('canvas');
    canvas.style.zIndex = 30;
    const gc = canvas.getContext('2d');
    element.appendChild(canvas);


    gc.fillStyle = 'burlywood';
    gc.strokeStyle = 'rgba(0, 0, 0, 1)';
    gc.font = '48px Lato, sans-serif';
    gc.fillText(length,110,100);
    gc.strokeText(length,110,100);
    
}

class Seed{
    constructor(isStorage){
        let colors = ["burlywood","black"]
        this.ang = Math.random() * 360;
        this.horiPos = Math.random() *  50-10;
        this.vertPos = Math.random() *  50-10;
        this.color = colors[Math.round(getRandomNumber(0,1))];
        console.log(this.color);
        if(isStorage){
            this.pivot = getRandomNumber(30,100);
            this.topPivot = getRandomNumber(60,100);
        }
        else{
            this.pivot = getRandomNumber(40,130);
            let maxNumberTop = Math.sqrt(130 * 130 - this.pivot * this.pivot);
            this.topPivot = getRandomNumber(30,maxNumberTop);
        }
    }

    generateSeed(hole){
        let seed = document.createElement("div");
        seed.classList.add("seed");
        seed.style.top = this.topPivot + "px";
        seed.style.left = this.pivot +"px";
        seed.style.transform = "translate(" + this.horiPos + "px, " + this.vertPos + "px) rotate(" + this.ang + "deg)";
        seed.style.background = this.color;
        hole.appendChild(seed);
    }

}


class Hole{
    constructor(numSeeds,side,index){
        this.element = this.generateElement();
        this.numSeeds = numSeeds;
        this.seeds = []
        this.side = side;
        this.index = index;
        for (let i = 0; i < numSeeds; i++){
            this.seeds.push(new Seed());
        }
    }

    attachMultiplayerClickEvent(){
        this.element.addEventListener("click",(evt) => {
            if(mancala.currentPlayer != this.side || !mancala.gameOnGoing)
                return;
            playSeedServer(this.index);
        })
    }

    attachHoverSeedsEvent(){
        this.element.addEventListener("mouseleave",(evt) => {
            removeHovers(this.element);
            this.element.style.backgroundColor = "white";
        })
        this.element.addEventListener("mouseover",(evt) =>{
            if(mancala.currentPlayer != this.side)
                return;

            drawCanvasSeeds(this.element,this.seeds.length);
            
            
            
            this.element.style.background = "grey";
        })
    }

    attachSowEvent(){
        this.element.addEventListener("click",(evt) => {
            if(mancala.currentPlayer == this.side && mancala.gameOnGoing){
                mancala.sow(this);
                if(mancala.ai != undefined){
                    while(mancala.currentPlayer == 0){
                        if(mancala.checkPlayerWin() || mancala.checkOpponentWin()){
                            mancala.endGame();
                            mancala.winningMessageHTML();
                            break;
                        }
                        else
                            mancala.sow(mancala.ai.chooseHole());
                    }
                }
                
                if(mancala.checkPlayerWin() || mancala.checkOpponentWin()){
                    mancala.endGame();
                    mancala.winningMessageHTML();
                }
                
                mancala.updatePlayerHTML();
                mancala.updateMancala();
            }
            else if(!mancala.gameOnGoing){
                mancala.saveScore();
                mancala.resetMancala();
                mancala.updatePlayerHTML();
                mancala.updateMancala();
            }
        })
    }

    generateElement(){
        let hole = document.createElement("div");
        hole.classList.add("hole");
        return hole;
    }

    //player 1 = us, player 0 = opponent
    generateSeedsHTML(player){
        let currentSide = (player == 1) ? "player-row" : "opponent-row";
        for(let i = 0; i < this.seeds.length; i++){
            this.seeds[i].generateSeed(this.element);
        }
        document.getElementsByClassName(currentSide)[0].appendChild(this.element);
    }

    addSeed(){
        this.seeds.push(new Seed());
    }

    removeSeed(n){
        for(let i = 0; i < n; i++)
            this.seeds.pop();
    }

    clearHole(){
        while(this.seeds.length > 0){
            this.seeds.pop();
        }
        this.clearHTML();
    }

    clearHTML(){
        while(this.element.children.length > 0){
            this.element.children[0].remove();
        }
    }

    updateHTML(){
        for (let i = 0; i < this.seeds.length; i++){
            this.seeds[i].generateSeed(this.element);
        }
    }

    reset(){
        this.clearHole();
        for(let i = 0; i < this.numSeeds;i++){
            this.addSeed();
        }
    }
    
}

class Row{
    constructor(numHoles,numSeeds,player){
        this.holes = []
        for (let i = 0; i < numHoles; i++){
            this.holes.push(new Hole(numSeeds,player,i));
            if(multiplayer)
                this.holes[i].attachMultiplayerClickEvent();
            else
                this.holes[i].attachSowEvent();
            this.holes[i].generateSeedsHTML(player);
            this.holes[i].attachHoverSeedsEvent();
        }
    }

    clearHTML(){
        for (let i = 0; i < this.holes.length; i++){
            this.holes[i].clearHTML();
        }
    }

    clearRow(){
        for (let i = 0; i < this.holes.length; i++){
            this.holes[i].clearHole();
        }
    }

    updateHTML(){
        for (let i = 0; i < this.holes.length; i++){
            this.holes[i].updateHTML();
        }
    }
    reset(){
        for(let i = 0; i < this.holes.length; i++){
            this.holes[i].reset();
        }
    }
    delete(){
        for(let i = 0; i < this.holes.length; i++){
            this.holes[i].element.remove();
        }
    }
    getTotalSeeds(){
        let total = 0;
        for(let i = 0; i < this.holes.length; i++){
            total += this.holes[i].seeds.length;
        }
        return total;
    }

}

class Storage{
    constructor(player){
        this.player = player;
        this.seeds = [];
        let currentClass = (player == 1) ? "player-storage" : "opponent-storage";
        this.element = document.getElementsByClassName(currentClass)[0];
    }

    addSeed(){
        this.seeds.push(new Seed(true));
    }

    addSeedHTML(){
        for (let i = 0; i < this.seeds.length; i++){
            this.seeds[i].generateSeed(this.element);
        }
    }

    clearStorage(){
        while(this.seeds.length > 0)
            this.seeds.pop();
        this.clearHTML();
    }

    clearHTML(){
        while(this.element.children.length > 0){
            this.element.children[0].remove();
        }
    }

    updateHTML(){
        this.clearHTML();
        this.addSeedHTML();
    }

    reset(){
        this.clearStorage();
    }
    
}


class Player{
    constructor(numHoles,numSeeds,player){
        this.row = new Row(numHoles,numSeeds,player);
        this.storage = new Storage(player);
        this.score = 0;
        console.log("Hey I'm Player", player, "and I've just been created. I hope we can get along :) Oh I already forgot, my score " + this.score);
    }

    getScore(){
        this.score = this.storage.seeds.length;
        return this.score;
    }

    clearHTML(){
        this.row.clearHTML();
    }

    updateHTML(){
        this.clearHTML();
        this.row.updateHTML();
        this.storage.updateHTML();
    }

    reset(){
        this.row.reset();
        this.storage.reset();
    }
    delete(){
        this.storage.reset();
        this.row.delete();
    }
}

class AI{
    constructor(aiLevel,opponent){
        this.aiLevel = aiLevel;
        this.opponent = opponent;
    }
    
    //In a first moment the AI chooses it randomly
    chooseHole(){
        switch(this.aiLevel){
            case 1:
                let validHoles = []
                for(const hole of this.opponent.row.holes){
                    if(hole.seeds.length != 0){
                        validHoles.push(hole);
                    }
                }
                let index = Math.floor(Math.random() * (validHoles.length - 0) + 0);
                return validHoles[index];
                
        }
    }
}

//aiLevel, 0 means no AI
class Mancala{
    constructor(numHoles,numSeeds,aiLevel){
        this.numHoles = numHoles;
        this.numSeeds = numSeeds;
        this.gameOnGoing = true;
        this.player = new Player(numHoles,numSeeds,1);
        this.opponent = new Player(numHoles,numSeeds,0);
        if(aiLevel != 0)
            this.ai = new AI(aiLevel,this.opponent);
        this.currentPlayer = 1;
    }

    setCurrentPlayer(player){
        this.currentPlayer = player;
    }

    clearMancalaHTML(){
        this.player.clearHTML();
        this.opponent.clearHTML();
    }

    updateMancala(){
        this.clearMancalaHTML();
        this.player.updateHTML();
        this.opponent.updateHTML();
    }

    //returns 1 if us and 0 if opponent
    getPlayerByHole(hole){
        return (this.player.row.holes.indexOf(hole) >= 0) ? 1 : 0
    }

    sowPlayerRow(numSeeds,index){
        let i = index;
        for(; i < this.player.row.holes.length; i++){
            if (numSeeds == 0)
                break;
            this.player.row.holes[i].addSeed();
            numSeeds--;
        }
        i--;
        return [numSeeds,i];
    }

    sowOpponentRow(numSeeds,index){
        let i = index;
        for(; i >= 0; i--){
            if (numSeeds == 0)
                break;
            this.opponent.row.holes[i].addSeed();
            numSeeds--;
        }
        i++;
        return [numSeeds,i];
    }
    lastHoleSteal(player, index){
        let lastHole = (player == 1) ? this.player.row.holes : this.opponent.row.holes;
        let inverseLastHole = (player == 1) ? this.opponent.row.holes : this.player.row.holes;
        let seedsToStorage = 0;
        let lastHoleSeeds = lastHole[index].seeds.length;
        //if it was an empty house
        if(lastHoleSeeds == 1){
            seedsToStorage++;
            lastHole[index].clearHole();
            seedsToStorage += inverseLastHole[index].seeds.length;
            inverseLastHole[index].clearHole();
            if(player == 1){
                for(let i = 0; i < seedsToStorage; i++){
                    this.player.storage.addSeed();
                }
            }
            else{
                for(let i = 0; i < seedsToStorage; i++){
                    this.opponent.storage.addSeed();
                }
            }
        }
    }


    sow(hole){
        let lastSowedHoleWasStorage = false;
        let player = this.getPlayerByHole(hole);
        let callingPlayer = player; // save so we can decide if we are able to sow a storage
        let toSowHoleIndex = player == 1 ? this.player.row.holes.indexOf(hole) + 1 : this.opponent.row.holes.indexOf(hole) - 1;
        let toSowSeeds = hole.seeds.length;
        if(toSowSeeds == 0)
        {
            console.log("Invalid Play");
            return;
        }
        hole.clearHole();
        while(toSowSeeds > 0){
            let lastHouseIndex; //index to check the last sowed house so we can decide if it was empty or not
            [toSowSeeds,lastHouseIndex] = (player == 1) ? this.sowPlayerRow(toSowSeeds,toSowHoleIndex) : this.sowOpponentRow(toSowSeeds,toSowHoleIndex);
            if(toSowSeeds == 0){
                if(player == callingPlayer){
                    this.lastHoleSteal(player,lastHouseIndex);
                }
                break;
            }
            

            //verificar se pode semear armazem
            if(player == 1 && callingPlayer){
                this.player.storage.addSeed();
                toSowSeeds--;
            }
            if(player == 0 && callingPlayer == 0){
                this.opponent.storage.addSeed();
                toSowSeeds--;
            }
            //---

            //se o ultimo semeio for no armazem
            if(toSowSeeds == 0){
                lastSowedHoleWasStorage = true;
                break;
            }
            toSowHoleIndex = (player == 1) ? (this.numHoles - 1) : 0;
            player = (player == 1) ? 0 : 1;
        }

        //se a ultima semente for no nosso lado podemos continuar a jogar >_<
        if(!lastSowedHoleWasStorage){
            this.currentPlayer = !this.currentPlayer;
        }   
    }

    checkOpponentWin(){
        for(const hole of this.opponent.row.holes){
            if(hole.seeds.length != 0)
                return false;
        }
        return true;
    }

    checkPlayerWin(){
        for(const hole of this.player.row.holes){
            if(hole.seeds.length != 0)
                return false;
        }
        return true;
    }
    
    updatePlayerHTML(){
        document.getElementById("player-turn").innerHTML = (this.currentPlayer == 1) ? "Player's turn" : "Opponent turn";
    }

    winningMessageHTML(){
        document.getElementById("winner").innerHTML = this.player.getScore() > this.opponent.getScore() ? "Player Wins" : "Opponent Wins";
    }

    saveScore(){
        let playerScore = localStorage.getItem("playerScore");
        let opponentScore = localStorage.getItem("opponentScore");
        let playerGames = localStorage.getItem("playerGames");
        let opponentGames = localStorage.getItem("opponentGames");
        if(playerScore == null){
            playerScore = 0;
            localStorage.setItem("playerScore",0);
        }
        if(opponentScore == null){
            opponentScore = 0;
            localStorage.setItem("opponentScore",0);
        }
        if(playerGames == null){
            playerGames = 0;
            localStorage.setItem("playerGames",0);
        }
        if(opponentGames == null){
            opponentGames = 0;
            localStorage.setItem("opponentGames",0);
        }



        if(this.player.getScore() > this.opponent.getScore()){
            localStorage.setItem("playerScore",String(Number(playerScore) + 1));
        }
        else if(this.player.getScore() < this.opponent.getScore()){
            localStorage.setItem("opponentScore",String(Number(opponentScore) + 1));
        }

        localStorage.setItem("playerGames",String(Number(playerGames) + 1));
        localStorage.setItem("opponentGames",String(Number(opponentGames) + 1));

        let table = document.getElementById("ranking-table");
        if(table == null){
            table = document.createElement("TABLE");
            table.id = "ranking-table";
        }
        var row = document.createElement("TR");
        table.append(row);
        var foo1 = document.createElement("TD");
        var playerNameTable = document.createTextNode("Player");
        foo1.append(playerNameTable);
        row.append(foo1);
        var foo2 = document.createElement("TD");
        var playerGamesTable = document.createTextNode(playerScore);
        foo2.append(playerGamesTable);
        row.append(foo2);
        var foo3 = document.createElement("TD");
        var playerScoreTable = document.createTextNode(playerGames);
        foo3.append(playerScoreTable);
        row.append(foo3);
    }

    resetMancala(){
        this.player.reset()
        this.opponent.reset();
        this.gameOnGoing = true;
        this.currentPlayer = 1;
        document.getElementById("winner").innerHTML = "";
    }

    changeTurns(){
        mancala.currentPlayer = !mancala.currentPlayer;
    }

    endGame(){
        this.gameOnGoing = false;
        for(let i = 0; i < this.player.row.getTotalSeeds(); i++){        
            this.player.storage.addSeed();
        }
        this.player.row.clearRow();
        for(let i = 0; i < this.opponent.row.getTotalSeeds(); i++){
            this.opponent.storage.addSeed();
        }
        this.opponent.row.clearRow();
    }

    setRowSeeds(seeds,player){
        if(player == 1){
            for(let i = 0; i < this.player.row.holes.length; i++)
            {
                this.player.row.holes[i].clearHole();
                for(let j = 0; j < seeds[i]; j++)
                    this.player.row.holes[i].addSeed();
            }
        }
        else{
            for(let i = 0; i < this.opponent.row.holes.length; i++){
                this.opponent.row.holes[i].clearHole();
                for(let j = 0; j < seeds[i]; j++)
                    this.opponent.row.holes[i].addSeed();
            }
        }
    }

    delete(){
        this.player.delete();
        this.opponent.delete();
        document.getElementById("player-turn").innerHTML = "";

    }

}

class Configuration{
    constructor()
    {
        this.numHoles = document.getElementById("numHoles").value;
        this.numSeeds = document.getElementById("numSeeds").value;
        this.aiLevel = Number(document.getElementById("aiLevel").value);
    }
}


var mancala = null;
let multiplayer = false;

function deleteMancala(){
    mancala.delete();
    mancala = null;
}

function createMancala(){
    if(mancala != null)
        deleteMancala();
    let configuration = new Configuration();
    mancala = new Mancala(configuration.numHoles,configuration.numSeeds,configuration.aiLevel);
}