const sendRequest = (method, url, data) => {
    return fetch(url,{
        method: method,
        body: JSON.stringify(data),
        mode: 'cors',
    })
    .then(response => {
        if(response.status >= 400) {//something went wrong
            return response.json().then(errData => {
                const error = new Error('Fucked up');
                error.data = errData;
                throw error;
            });
        }
        return response.json();
    });

};




async function register(nick,password,port){
    try{
        const req = await sendRequest('POST',`http://twserver.alunos.dcc.fc.up.pt:${port}/register`,{
            "nick": nick, "password": password
        });
        return "Login Successfully";
    } catch(err){
        return err.data.error;
    }
}

async function ranking(host){
    try{
        const req = await sendRequest('POST',host,{});
        return req;
    }catch(err){
        return err.data;
    }
}


async function join(nick,password,size,initial){
    try{
        const req = await sendRequest('POST','http://twserver.alunos.dcc.fc.up.pt:8008/join',	{"group": 420, "nick": nick, "password": password, "size": size, "initial" : initial });
        return req.game;
    } catch(err){
        return err.data;
    }
}

async function leave(nick,password,game){
    try{
        const req = await sendRequest('POST','http://twserver.alunos.dcc.fc.up.pt:8008/leave',{"nick" : nick, "password" : password, "game" : game});
        return req;
    } catch(err){
        return err.data;
    }
}

async function notify(nick,password,move,game){
    try{
        const req = sendRequest('POST','http://twserver.alunos.dcc.fc.up.pt:8008/notify',{
            "nick" : nick,"password" : password, "game" : game, "move" : move
        });
        return nick + " played on hole " + move; 
    } catch(err){
        return err.data;
    }
}


async function update(nick,game,func){


    let url = new URL('http://twserver.alunos.dcc.fc.up.pt:8008/update')
    url.searchParams.set('nick',nick);
    url.searchParams.set('game',game);



    const evtSource = new EventSource(url);
    evtSource.onmessage = func
    evtSource.onerror = console.log("ERROR");

}


//curl -H 'Content-Type: application/json' -d '{"group": 99, "nick": "zp", "password": "secret", "size": 6, "initial" 4 }' --request POST --url http://127.0.0.1:9100/join
async function joinNode(){
    try{
        const req = await sendRequest('POST','http://twserver.alunos.dcc.fc.up.pt:9100/join',{"group": 99, "nick": document.getElementById("username").value, "password": document.getElementById("password").value, 
        "size": Number(document.getElementById("numHoles").value), 
        "initial" : Number(document.getElementById("numSeeds").value)});
        console.log(req);
        return req;
    }catch(err){
        return err.data;
    }
}